<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'SomeUserFormController@step1')->name('step1');
    Route::get('/step1', 'SomeUserFormController@step1')->name('step1');
    Route::post('/step1', 'SomeUserFormController@step1Form')->name('step1Form');

    Route::group(['middleware' => ['check_form_completion']], function () {
        Route::get('/step2', 'SomeUserFormController@step2')->name('step2');
        Route::post('/step2', 'SomeUserFormController@step2Form')->name('step2Form');
        Route::get('/step3', 'SomeUserFormController@step3')->name('step3');
        Route::post('/step3', 'SomeUserFormController@step3Form')->name('step3Form');
    });
});
