<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// reset table
		DB::table('users')->truncate();

		// generate records
		DB::table('users')->insert([
			[
				'name' => "Test Account",
				'email' => "test@email.com",
				'password' => bcrypt("12345678"),
			],
		]);
    }
}
