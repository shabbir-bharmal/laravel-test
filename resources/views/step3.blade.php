@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- SHOW SUCCESS / ERROR NOTIFICATIONS -->
        @include('flash::message')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Step3</div>

                    <div class="panel-body">
                        <!-- FORM OPEN -->
                        {{ Form::open(['route' => 'step3Form', 'id' => 'step3Form', 'class'=>'form-horizontal']) }}
                        {{ Form::hidden('id', ($data? $data->id : 0)) }}

                        <!-- DATA 4 INPUT -->
                        <div class="form-group">
                            {{ Form::label('data4','Data field 4:', ['class'=>'col-md-4 control-label']) }}

                            <div class="col-md-6">
                                {{ Form::text('data4', ($data? $data->data4 : old('data4')), ['id' =>'data4', 'class' => 'form-control', 'autofocus']) }}

                                @if ($errors->has('data4'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data4') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- SUBMIT + BACK INPUTS -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                {{ link_to_route('step2', 'Back', [], ['class' => 'btn btn-primary']) }}
                                {{ Form::submit('Next', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>

                        <!-- FORM CLOSE -->
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <!-- STEP 3 FORM VALIDATIONS -->
    <script type="text/javascript">
        $('document').ready(function () {
            $("#step3Form").validate({
                rules: {
                    'data4': "required"
                }
            });
        });
    </script>
@endsection
