@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- SHOW SUCCESS / ERROR NOTIFICATIONS -->
        @include('flash::message')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Step2</div>

                    <div class="panel-body">
                        <!-- FORM OPEN -->
                        {{ Form::open(['route' => 'step2Form', 'id' => 'step2Form', 'class'=>'form-horizontal']) }}
                        {{ Form::hidden('id', ($data? $data->id : 0)) }}

                        <!-- DATA 3 INPUT -->
                        <div class="form-group">
                            {{ Form::label('data3','Data field 3:', ['class'=>'col-md-4 control-label']) }}

                            <div class="col-md-6">
                                {{ Form::text('data3', ($data? $data->data3 : old('data3')), ['id' =>'data3', 'class' => 'form-control', 'autofocus']) }}

                                @if ($errors->has('data3'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data3') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- SUBMIT + BACK INPUTS -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                {{ link_to_route('step1', 'Back', [], ['class' => 'btn btn-primary']) }}
                                {{ Form::submit('Next', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>

                        <!-- FORM CLOSE -->
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <!-- STEP 2 FORM VALIDATIONS -->
    <script type="text/javascript">
        $('document').ready(function () {
            $("#step2Form").validate({
                rules: {
                    'data3': "required"
                }
            });
        });
    </script>
@endsection
