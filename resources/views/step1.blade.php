@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- SHOW SUCCESS / ERROR NOTIFICATIONS -->
        @include('flash::message')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Step1</div>

                    <div class="panel-body">
                        <!-- FORM OPEN -->
                        {{ Form::open(['route' => 'step1Form', 'id' => 'step1Form', 'class'=>'form-horizontal']) }}
                        {{ Form::hidden('id', ($data? $data->id : 0)) }}

                        <!-- DATA 1 INPUT -->
                        <div class="form-group">
                            {{ Form::label('data1','Data field 1:', ['class'=>'col-md-4 control-label']) }}

                            <div class="col-md-6">
                                {{ Form::text('data1', ($data? $data->data1 : old('data1')), ['id' =>'data1', 'class' => 'form-control', 'autofocus']) }}

                                @if ($errors->has('data1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- DATA 2 INPUT -->
                        <div class="form-group">
                            {{ Form::label('data2','Data field 2:', ['class'=>'col-md-4 control-label']) }}

                            <div class="col-md-6">
                                {{ Form::text('data2', ($data? $data->data2 : old('data2')), ['id' =>'data2', 'class' => 'form-control']) }}

                                @if ($errors->has('data2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- SUBMIT INPUT -->
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                {{ Form::submit('Next', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                    </div>

                    <!-- FORM CLOSE -->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <!-- STEP 1 FORM VALIDATIONS -->
    <script type="text/javascript">
        $('document').ready(function () {
            $("#step1Form").validate({
                rules: {
                    'data1': "required",
                    'data2': "required"
                }
            });
        });
    </script>
@endsection