<?php

namespace App\Http\Controllers;

use App\Http\Requests\Step1FormPost;
use App\Http\Requests\Step2FormPost;
use App\Http\Requests\Step3FormPost;
use App\Repositories\SomeUserFormRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class SomeUserFormController
 * @package App\Http\Controllers
 */
class SomeUserFormController extends Controller
{
    /**
     * @var SomeUserFormRepository
     */
    protected $formRepository;

    /**
     * SomeUserFormController constructor.
     * @param SomeUserFormRepository $formRepository
     */
    public function __construct(SomeUserFormRepository $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * GET: step1
     * @return $this
     */
    public function step1()
    {
        $data = $this->formRepository->getData();
        return view('step1')->with(compact('data'));
    }

    /**
     * POST: Step1
     * @param Step1FormPost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function step1Form(Step1FormPost $request)
    {
        $inputs            = $request->only('data1', 'data2', 'id');
        $inputs['user_id'] = Auth::id();
        $response          = $this->formRepository->save($inputs);
        if ($response) {
            flash('Step1 completed successfully.')->success();
        } else {
            flash('Error completing Step1.')->error();
        }
        return redirect()->route('step2');
    }

    /**
     * GET: step2
     * @return $this
     */
    public function step2()
    {
        $data = $this->formRepository->getData();
        return view('step2')->with(compact('data'));
    }

    /**
     * POST: step2
     * @param Step2FormPost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function step2Form(Step2FormPost $request)
    {
        $inputs            = $request->only('data3', 'id');
        $inputs['user_id'] = Auth::id();
        $response          = $this->formRepository->save($inputs);
        if ($response) {
            flash('Step2 completed successfully.')->success();
        } else {
            flash('Error completing Step2.')->error();
        }
        return redirect()->route('step3');
    }

    /**
     * GET: step3
     * @return $this
     */
    public function step3()
    {
        $data = $this->formRepository->getData();
        return view('step3')->with(compact('data'));
    }

    /**
     * POST: step3
     * @param Step3FormPost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function step3Form(Step3FormPost $request)
    {
        $inputs            = $request->only('data4', 'id');
        $inputs['user_id'] = Auth::id();
        $response          = $this->formRepository->save($inputs);
        if ($response) {
            flash('Step3 completed successfully.')->success();
        } else {
            flash('Error completing Step3.')->error();
        }
        return redirect()->route('step1');
    }
}
