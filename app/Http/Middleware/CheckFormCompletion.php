<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\SomeUserFormRepository;

/**
 * Class CheckFormCompletion
 * @package App\Http\Middleware
 */
class CheckFormCompletion
{
    /**
     * @var SomeUserFormRepository
     */
    protected $formRepository;

    /**
     * CheckFormCompletion constructor.
     * @param SomeUserFormRepository $formRepository
     */
    public function __construct(SomeUserFormRepository $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->route()->getName(), ['step2', 'step3', 'step2Form', 'step3Form'])) {
            $data = $this->formRepository->getData();
            if (in_array($request->route()->getName(), ['step2', 'step2Form'])) {
                /* Verify Step 1 is completed. */
                if (!$data || empty($data->data1) || empty($data->data2)) {
                    flash('Please complete Step1, before moving forward.')->error();
                    return redirect()->route('step1');
                }
            }
            if (in_array($request->route()->getName(), ['step3', 'step3Form'])) {
                /* Verify Step 2 is completed. */
                if (empty($data->data3)) {
                    flash('Please complete Step2, before moving forward.')->error();
                    return redirect()->route('step2');
                }
            }
        }
        return $next($request);
    }
}
