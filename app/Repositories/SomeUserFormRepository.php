<?php
namespace App\Repositories;

use App\Data;
use Illuminate\Support\Facades\Auth;

/**
 * Class SomeUserFormRepository
 * @package App\Repositories
 */
class SomeUserFormRepository
{
    /**
     *
     * @var Data
     */
    protected $model;

    /**
     * SomeUserFormRepository  constructor.
     * @param Data $model
     */
    public function __construct(Data $model)
    {
        $this->model = $model;
    }

    /**
     * Get data for logged in user.
     * @return mixed
     */
    public function getData()
    {
        return $this->model->where('user_id', Auth::id())->first();
    }

    /**
     * Save step1 / step2 / step3 data.
     * @param $inputs
     * @return bool
     */
    public function save($inputs)
    {
        if ($inputs['id']) {
            return $this->model->find($inputs['id'])->update($inputs);
        } else {
            return $this->model->fill($inputs)->save();
        }
    }

}