<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Data
 * @package App
 */
class Data extends Model
{
    /**
     * @var string
     */
    protected $table = 'data';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var bool
     */
    public $timestamps = false;
}
